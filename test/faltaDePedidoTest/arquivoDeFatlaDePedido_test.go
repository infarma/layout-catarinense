package faltaDePedidoTest

import (
	"layout-catarinense/faltaDePedido"
	"testing"
)

func TestCabecalhoFaltaDePedido(t *testing.T){

	SequencialArquivo := int32(00001)
	IdentificadorTipoArquivo := "FAL"
	CodigoFornecedor := "XXXXXXXXX"
	CodigoCliente := "ZZZZZZ"
	NumeroPedido := int32(10001)
	IdentificadorRejeicaoPedido  := "1"
	DescricaoMotivoRejeicaoPedido := "SNAO ATINGIU O VALOR MINIMO"
	NumeroCGCCliente := "12345678911111"


	cabecalho := faltaDePedido.CabecalhoFaltaDePedido( SequencialArquivo , IdentificadorTipoArquivo , CodigoFornecedor , CodigoCliente, NumeroPedido, IdentificadorRejeicaoPedido , DescricaoMotivoRejeicaoPedido, NumeroCGCCliente )


	if cabecalho != "100001FALXXXXXXXXXZZZZZZ000100011SNAO ATINGIU O VALOR MINIMO   12345678911111" {
		t.Error("cabecalho não é compativel")
	}

}

func TestDetalheFaltaDePedido(t *testing.T){

	SequencialArquivo := int32(00002)
	CodigoProduto := "32111111"
	QuantidadeNaoAtendida  := int32(00002)
	DescricaoMotivoRejeicao  := "1010 00131849000000000000000000000000"

	detalhe := faltaDePedido.DetalheFaltaDePedido(SequencialArquivo, CodigoProduto, QuantidadeNaoAtendida, DescricaoMotivoRejeicao  )

	if detalhe != "20000232111111000021010 00131849000000000000000000000000             " {
		t.Error("detalhe não é compativel")
	}

}

func TestFinalizadorFaltaDePedido(t *testing.T){

	SequencialArquivo := int32(00002)
	NumeroTotalProdutosNaoAtendidos := int32(57)
	QuantidadeTotalProdutosNaoAtendidos  := int32(103)

	Finalizador := faltaDePedido.FinalizadorFaltaDePedido(SequencialArquivo, NumeroTotalProdutosNaoAtendidos, QuantidadeTotalProdutosNaoAtendidos)

	if Finalizador != "300002000570000103" {
		t.Error("Finalizador não é compativel")
	}

}
