package arquivoDePedidoTest

import (
	"layout-catarinense/arquivoDePedido"
	"os"
	"testing"
)

func TestGetStruct01(t *testing.T) {

	file, err := os.Open("../testFile/110101_22131849.EXT")
	if err != nil {
		t.Error("não abriu arquivo", err)
	}

	arquivo,err2 := arquivoDePedido.GetStruct(file)
	//REGISTRO TIPO 1 ( cabecalho)
	var cabecalho arquivoDePedido.RegistroCabecalho
	cabecalho.IdentificadorTipoRegistro = "1"
	cabecalho.SequencialArquivo = int32(10001)
	cabecalho.IdentificadorTipoArquivo = "PED"
	cabecalho.CodigoFornecedor = "053405477"
	cabecalho.CodigoCliente = "110101"
	cabecalho.NumeroPedido = int32(22131849)
	cabecalho.CodigoPromocao = int32(0000000001)
	cabecalho.NumeroCGCCliente = "00000000000000"

	if cabecalho.IdentificadorTipoRegistro != arquivo.RegistroCabecalho.IdentificadorTipoRegistro {
		t.Error("IdentificadorTipoRegistro não é compativel", err2)
	}
	if cabecalho.SequencialArquivo != arquivo.RegistroCabecalho.SequencialArquivo {
		t.Error("SequencialArquivo não é compativel", err2)
	}
	if cabecalho.IdentificadorTipoArquivo != arquivo.RegistroCabecalho.IdentificadorTipoArquivo {
		t.Error("IdentificadorTipoArquivo não é compativel", err2)
	}
	if cabecalho.CodigoFornecedor != arquivo.RegistroCabecalho.CodigoFornecedor {
		t.Error("CodigoFornecedor não é compativel", err2)
	}
	if cabecalho.CodigoCliente != arquivo.RegistroCabecalho.CodigoCliente {
		t.Error("CodigoCliente não é compativel", err2)
	}
	if cabecalho.NumeroPedido != arquivo.RegistroCabecalho.NumeroPedido {
		t.Error("NumeroPedido não é compativel", err2)
	}
	if cabecalho.CodigoPromocao != arquivo.RegistroCabecalho.CodigoPromocao {
		t.Error("CodigoPromocao não é compativel", err2)
	}
	if cabecalho.NumeroCGCCliente != arquivo.RegistroCabecalho.NumeroCGCCliente {
		t.Error("NumeroCGCCliente não é compativel", err2)
	}

	//REGISTRO TIPO 2 ( detalhe )
	var detalhe arquivoDePedido.RegistroDetalhe
	detalhe.IdentificadorTipoRegistro = "2"
	detalhe.SequencialArquivo = int32(00002)
	detalhe.CodigoProduto = "32111111"
	detalhe.QuantidadePerdida = int32(2)

	if detalhe.IdentificadorTipoRegistro != arquivo.RegistroDetalhe[0].IdentificadorTipoRegistro {
		t.Error("IdentificadorTipoRegistro não é compativel", err2)
	}
	if detalhe.SequencialArquivo != arquivo.RegistroDetalhe[0].SequencialArquivo {
		t.Error("SequencialArquivo não é compativel", err2)
	}
	if detalhe.CodigoProduto != arquivo.RegistroDetalhe[0].CodigoProduto {
		t.Error("CodigoProduto não é compativel", err2)
	}
	if detalhe.QuantidadePerdida != arquivo.RegistroDetalhe[0].QuantidadePerdida {
		t.Error("QuantidadePerdida não é compativel", err2)
	}

	//REGISTRO TIPO 3 ( finalizador )
	var finalizador arquivoDePedido.RegistroFinalizador
	finalizador.IdentificadorTipoRegistro = "3"
	finalizador.SequencialArquivo = int32(00002)
	finalizador.NumeroTotalProdutosPerdidos = int32(57)
	finalizador.QuantidadeTotalProdutosPerdidos =  int32(103)

	if finalizador.IdentificadorTipoRegistro != arquivo.RegistroFinalizador.IdentificadorTipoRegistro {
		t.Error("IdentificadorTipoRegistro não é compativel", err2)
	}

	if finalizador.SequencialArquivo != arquivo.RegistroFinalizador.SequencialArquivo {
		t.Error("SequencialArquivo não é compativel", err2)
	}

	if finalizador.NumeroTotalProdutosPerdidos != arquivo.RegistroFinalizador.NumeroTotalProdutosPerdidos {
		t.Error("NumeroTotalProdutosPerdidos não é compativel", err2)
	}

	if finalizador.QuantidadeTotalProdutosPerdidos != arquivo.RegistroFinalizador.QuantidadeTotalProdutosPerdidos {
		t.Error("QuantidadeTotalProdutosPerdidos não é compativel", err2)
	}


}