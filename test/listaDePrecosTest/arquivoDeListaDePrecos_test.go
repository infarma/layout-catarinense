package listaDePrecosTest

import (
	"fmt"
	"layout-catarinense/listaDePrecos"
	"testing"
	"time"
)

func TestCabecalhoDeFaltaDePedido(t *testing.T){

	SequencialArquivo := int32(00002)
	IdentificadorTipoArquivo := "333"
	CodigoFornecedor := "999999999"
	UnidadeFederacao := "22"
	DataGeracaoArquivoPreco := time.Date(2019, 10, 22, 12, 30, 0, 0, time.UTC)

	Cabecalho := listaDePrecos.CabecalhoListaDePrecos(SequencialArquivo, IdentificadorTipoArquivo, CodigoFornecedor, UnidadeFederacao, DataGeracaoArquivoPreco  )

	if Cabecalho != "1000023339999999992222102019" {
		fmt.Println("cabecalho = ", Cabecalho)
		fmt.Println("teste     =", "1000023339999999992222102019")
		t.Error("Cabecalho não é compativel")
	}

}

func TestDetalheListaDePrecos(t *testing.T){

	SequencialArquivo := int32(2)
	CodigoProduto := "88888888"
	PrecoUnitario := float32(55555.22)
	PercentualDesconto := float32(333.22)
	PercentualRepasseICMS := float32(333.22)
	PercentualICMS := float32(333.22)
	PercentualReducaoBaseICMS := float32(333.22)
	PrecoBaseCalculoICMSSubstituicaoTributaria := float32(55555.22)
	ValorICMSSubstituicaoTributaria := float32(55555.22)
	NumeroDiasParaPagamento := "333"
	CodigoEANProduto := "88888888888888888888"
	ProdutoHPC := "1"
	PercentualDescontoAdicionalPedidoEnviado := float32(333.22)
	ValorICMSSubstituicaoTributariaParaProdutosFarmaciaPopularBrasil := float32(55555.22)
	QunatidadeEstoqueDisponivel := int32(2)

	Detalhe := listaDePrecos.DetalheistaDePrecos(	SequencialArquivo, CodigoProduto, PrecoUnitario , PercentualDesconto, PercentualRepasseICMS , PercentualICMS , PercentualReducaoBaseICMS , PrecoBaseCalculoICMSSubstituicaoTributaria , ValorICMSSubstituicaoTributaria, NumeroDiasParaPagamento , CodigoEANProduto , ProdutoHPC, PercentualDescontoAdicionalPedidoEnviado, ValorICMSSubstituicaoTributariaParaProdutosFarmaciaPopularBrasil, QunatidadeEstoqueDisponivel)

	if Detalhe != "2000028888888855555223332233322333223332255555225555522333888888888888888888881333225555522000002" {
		t.Error("Detalhe não é compativel")
	}

}


func TestFinalizadorListaDePrecos(t *testing.T){

	SequencialArquivo := int32(00002)
	QuantidadeTotalRegistros := int32(00002)

	Finalizador := listaDePrecos.FinalizadorListaDePrecos(SequencialArquivo, QuantidadeTotalRegistros)

	if Finalizador != "30000200002" {
		t.Error("Finalizador não é compativel")
	}

}
