package arquivoDePedido

import (
	"bufio"
	"os"
)

type ArquivoDePedido struct {
	RegistroCabecalho   RegistroCabecalho   `json:"RegistroCabecalho"`
	RegistroDetalhe     []RegistroDetalhe   `json:"RegistroDetalhe"`
	RegistroFinalizador RegistroFinalizador `json:"RegistroFinalizador"`
}

func GetStruct(fileHandle *os.File) (ArquivoDePedido, error) {
	fileScanner := bufio.NewScanner(fileHandle)

	arquivo := ArquivoDePedido{}
	var err error
	for fileScanner.Scan() {
		runes := []rune(fileScanner.Text())
		identificador := string(runes[0:1])

		if identificador == "1" {
			err = arquivo.RegistroCabecalho.ComposeStruct(string(runes))
		} else if identificador == "2" {
			var registroDet RegistroDetalhe
			err = registroDet.ComposeStruct(string(runes))
			arquivo.RegistroDetalhe = append(arquivo.RegistroDetalhe, registroDet)
		} else if identificador == "3" {
			err = arquivo.RegistroFinalizador.ComposeStruct(string(runes))
		}
	}
	return arquivo, err
}
