package faltaDePedido

import "fmt"

type ArquivoDeFaltaDePedido struct {

	RegistroCabecalho    RegistroCabecalho    `json:"RegistroCabecalho"`
	RegistroDetalhe      []RegistroDetalhe    `json:"RegistroDetalhe"`
	RegistroFinalizador  RegistroFinalizador  `json:"RegistroFinalizador"`
}

// registro 1 RegistroCabecalho

func CabecalhoFaltaDePedido( SequencialArquivo int32, IdentificadorTipoArquivo string, CodigoFornecedor string,CodigoCliente string, NumeroPedido  int32, IdentificadorRejeicaoPedido   string, DescricaoMotivoRejeicaoPedido string, NumeroCGCCliente string ) string {

	Cabecalho := fmt.Sprint("1")
	Cabecalho += fmt.Sprintf("%05d", SequencialArquivo)
	Cabecalho += fmt.Sprintf("%03s", IdentificadorTipoArquivo)
	Cabecalho += fmt.Sprintf("%09s", CodigoFornecedor)
	Cabecalho += fmt.Sprintf("%06s", CodigoCliente)
	Cabecalho += fmt.Sprintf("%08d", NumeroPedido)
	Cabecalho += fmt.Sprintf("%01s", IdentificadorRejeicaoPedido)
	Cabecalho += fmt.Sprintf("%-30s", DescricaoMotivoRejeicaoPedido)
	Cabecalho += fmt.Sprintf("%014s", NumeroCGCCliente)

	return Cabecalho
}

// registro 2 RegistroDetalhe

func DetalheFaltaDePedido( SequencialArquivo int32, CodigoProduto string, QuantidadeNaoAtendida  int32, DescricaoMotivoRejeicao  string) string {

	Detalhe := fmt.Sprint("2")
	Detalhe += fmt.Sprintf("%05d",SequencialArquivo )
	Detalhe += fmt.Sprintf("%08s",CodigoProduto )
	Detalhe += fmt.Sprintf("%05d",QuantidadeNaoAtendida )
	Detalhe += fmt.Sprintf("%-50s",DescricaoMotivoRejeicao )

	return Detalhe
}

// registro 3 RegistroFinalizador


func FinalizadorFaltaDePedido( SequencialArquivo int32, NumeroTotalProdutosNaoAtendidos int32, QuantidadeTotalProdutosNaoAtendidos int32) string {

	Finalizador := fmt.Sprint("3")
	Finalizador += fmt.Sprintf("%05d",SequencialArquivo )
	Finalizador += fmt.Sprintf("%05d",NumeroTotalProdutosNaoAtendidos )
	Finalizador += fmt.Sprintf("%07d",QuantidadeTotalProdutosNaoAtendidos )

	return Finalizador
}






