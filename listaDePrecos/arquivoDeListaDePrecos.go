package listaDePrecos

import (
	"fmt"
	"regexp"
	"strings"
	"time"
)

type ArquivoDeListaDePrecos struct {

	RegistroCabecalho    RegistroCabecalho    `json:"RegistroCabecalho"`
	RegistroDetalhe      []RegistroDetalhe    `json:"RegistroDetalhe"`
	RegistroFinalizador  RegistroFinalizador  `json:"RegistroFinalizador"`
}

// registro 1 RegistroCabecalho

func CabecalhoListaDePrecos(  SequencialArquivo int32, IdentificadorTipoArquivo string, CodigoFornecedor string , UnidadeFederacao string , DataGeracaoArquivoPreco time.Time  ) string {

	Cabecalho := fmt.Sprint("1")
	Cabecalho += fmt.Sprintf("%05d", SequencialArquivo )
	Cabecalho += fmt.Sprintf("%-3s", IdentificadorTipoArquivo )
	Cabecalho += fmt.Sprintf("%-9s", CodigoFornecedor )
	Cabecalho += fmt.Sprintf("%-2s", UnidadeFederacao )
	str := DataGeracaoArquivoPreco.String()
	x := str[0:10]
	re := regexp.MustCompile(`:|-|\s`)
	data := re.ReplaceAllLiteralString(x, "")
	dia := data[6:8]
	mes := data[4:6]
	ano := data[0:4]
	datainvertida := dia + mes + ano
	Cabecalho += fmt.Sprintf("%08s", datainvertida)

	return Cabecalho
}

// registro 2 RegistroDetalhe

func DetalheistaDePrecos( SequencialArquivo int32, CodigoProduto string, PrecoUnitario float32, PercentualDesconto float32, PercentualRepasseICMS float32, PercentualICMS float32, PercentualReducaoBaseICMS float32, PrecoBaseCalculoICMSSubstituicaoTributaria float32, ValorICMSSubstituicaoTributaria float32, NumeroDiasParaPagamento string, CodigoEANProduto string, ProdutoHPC string, PercentualDescontoAdicionalPedidoEnviado float32, ValorICMSSubstituicaoTributariaParaProdutosFarmaciaPopularBrasil float32, QunatidadeEstoqueDisponivel int32 ) string {

	Detalhe := fmt.Sprint("2")
	Detalhe += fmt.Sprintf("%05d", SequencialArquivo )
	Detalhe += fmt.Sprintf("%-8s", CodigoProduto )
	Detalhe += fmt.Sprintf("%07s", strings.Replace(fmt.Sprintf("%.2f", PrecoUnitario), ".", "", 1))
	Detalhe += fmt.Sprintf("%05s", strings.Replace(fmt.Sprintf("%.2f", PercentualDesconto), ".", "", 1))
	Detalhe += fmt.Sprintf("%05s", strings.Replace(fmt.Sprintf("%.2f", PercentualRepasseICMS), ".", "", 1))
	Detalhe += fmt.Sprintf("%05s", strings.Replace(fmt.Sprintf("%.2f", PercentualICMS), ".", "", 1))
	Detalhe += fmt.Sprintf("%05s", strings.Replace(fmt.Sprintf("%.2f", PercentualReducaoBaseICMS), ".", "", 1))
	Detalhe += fmt.Sprintf("%07s", strings.Replace(fmt.Sprintf("%.2f", PrecoBaseCalculoICMSSubstituicaoTributaria), ".", "", 1))
	Detalhe += fmt.Sprintf("%07s", strings.Replace(fmt.Sprintf("%.2f", ValorICMSSubstituicaoTributaria), ".", "", 1))
	Detalhe += fmt.Sprintf("%-3s", NumeroDiasParaPagamento )
	Detalhe += fmt.Sprintf("%-20s", CodigoEANProduto )
	Detalhe += fmt.Sprintf("%01s", ProdutoHPC )
	Detalhe += fmt.Sprintf("%05s", strings.Replace(fmt.Sprintf("%.2f", PercentualDescontoAdicionalPedidoEnviado), ".", "", 1))
	Detalhe += fmt.Sprintf("%07s", strings.Replace(fmt.Sprintf("%.2f", ValorICMSSubstituicaoTributariaParaProdutosFarmaciaPopularBrasil), ".", "", 1))
	Detalhe += fmt.Sprintf("%06d", QunatidadeEstoqueDisponivel )

	return Detalhe
}

// registro 3 RegistroFinalizador

func FinalizadorListaDePrecos(SequencialArquivo int32, QuantidadeTotalRegistros int32   ) string {

	Finalizador := fmt.Sprint("3")
	Finalizador += fmt.Sprintf("%05d",SequencialArquivo )
	Finalizador += fmt.Sprintf("%05d",QuantidadeTotalRegistros )



	return Finalizador
}